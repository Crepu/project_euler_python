# project_euler_python

## Recomendaciones para este proyecto

1.- Instalar emacs
2.- Instalar python si no lo tienes. Generalmente en cualquier versión de Linux tendrás la versión 2.7 instalada. Si quieres actualizarte a python 3, lo que te recomiendo encareidamente, aplica `sudo apt install python3`.
2.5.- Me resultó bastente útil usar la herramienta `update-alternatives` para cambiar el binario de `python` que pPor defecto viene con la versión 2.
3.- Instala pip para instalar paquetes de python, metele `sudo apt install python3-pip`
4.- Instalar jupyter notebooks, ponle un `sudo apt install jupyter`
5.- Disfrute mientras programa y cuide su postura
